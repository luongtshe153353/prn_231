﻿namespace OiHouseCoffee.Request
{
    public class ProductSearchRequest
    {
        public int? Id { get; set; }
        public int? CategoryId { get; set; }
        public string? ProductName { get; set; }
        public int? Price { get; set; }
    }
}
