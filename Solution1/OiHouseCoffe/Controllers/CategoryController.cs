﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Data_Layer.Repository;
using OiHouseCoffee.Request;

namespace OiHouseCoffee.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {

        private CategoryRepository categoryRepository = new CategoryRepository();


        [HttpGet]
        [Route("search")]
        public IActionResult search()
        {
            List<FoodCategory> categories = (List<FoodCategory>)categoryRepository.GetAll();
            return Ok(categories);
        }
    }
}
