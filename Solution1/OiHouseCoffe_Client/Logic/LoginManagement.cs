﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Request;

namespace OiHouseCoffe_Client.Logic
{
    public class LoginManagement
    {
        private readonly HttpClient client = null;
        private string url = "";

        public LoginManagement()
        {
            client = new HttpClient();
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);
            client.DefaultRequestHeaders.Accept.Clear();
            url = "http://localhost:5123/api/login";
        }

        public async Task<Account> Login(AccountRequest account)
        {
            var json = JsonConvert.SerializeObject(account);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync($"{url}/signin", content);
            if(response.IsSuccessStatusCode)
            {
                string data = await response.Content.ReadAsStringAsync();
                var options = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };
                if (String.IsNullOrEmpty(data))
                {
                    return null;
                }
                else
                {
                    Account acc1 = System.Text.Json.JsonSerializer.Deserialize<Account>(data, options);
                    return acc1;
                }

            }
            else
            {
                return null;
            }
        }
    }
}
