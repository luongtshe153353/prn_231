﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Data_Layer.Repository;
using OiHouseCoffee.Request;
using OiHouseCoffee.Service;
using System.Text.Json;
using OiHouseCoffee.Response;

namespace OiHouseCoffee.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {

        private AccountRepository accountRepository = new AccountRepository();

        private EmployeeService employeeService = new EmployeeService();


        [HttpPost]
        [Route("search")]
        public IActionResult search([FromBody] EmployeeSearchRequest employeeSearchRequest)
        {
                List<Account> accounts = (List<Account>)accountRepository.FindAllBy(employeeSearchRequest);
                return Ok(accounts);  
        }

        [HttpPost]
        [Route("addEmployee")]
        public IActionResult addEmployee([FromBody] Account account)
        {
            ResponseCommon reponse = new ResponseCommon();
            try
            {
                try
                {
                   return Ok(employeeService.addEmployee(account));
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
            catch (Exception e)
            {
                return Ok(Contant.ERROR);
            }
        }

        [HttpPut]
        [Route("updateEmployee")]
        public IActionResult updateEmployee([FromBody] Account account)
        {
            try
            {
                try
                {
                    return Ok(employeeService.updateEmployee(account));
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("updateStatusAccount")]
        public IActionResult deactiveAccount([FromBody] RequestUpdateStatus requestUpdateStatus)
        {
            try
            {
                try
                {
                    return Ok(employeeService.updateStatusUser(requestUpdateStatus.id, requestUpdateStatus.status));
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
