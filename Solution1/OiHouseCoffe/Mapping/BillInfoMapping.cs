﻿using AutoMapper;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Response;

namespace OiHouseCoffee.Mapping
{
    public class BillInfoMapping : Profile
    {
        public BillInfoMapping()
        {
            CreateMap<BillInfoReponse, BillInfo>();
            CreateMap<BillInfo, BillInfoReponse>();
        }

    }
}
