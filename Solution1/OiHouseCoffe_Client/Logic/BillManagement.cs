﻿using AutoMapper;
using Newtonsoft.Json;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Mapping;
using OiHouseCoffee.Request;
using OiHouseCoffee.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace OiHouseCoffe_Client.Logic
{
    internal class BillManagement
    {
        private readonly HttpClient client = null;
        private string url = "";
        IMapper mapper;

        public BillManagement()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new BillMapping());
                cfg.AddProfile(new BillInfoMapping());
            });
            mapper = config.CreateMapper();
            client = new HttpClient();
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);
            client.DefaultRequestHeaders.Accept.Clear();
            url = "http://localhost:5123/api/bill";
        }

        public async Task<List<Bill>> Search(BillSearchRequest billSearchRequest)
        {
            var json = JsonConvert.SerializeObject(billSearchRequest);

            var content = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync($"{url}/search", content);

            string strdata = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            List<BillResponse> listBills = System.Text.Json.JsonSerializer.Deserialize<List<BillResponse>>(strdata, options).ToList();
            return mapper.Map<List<Bill>>(listBills);
        }

        public async Task<List<BillInfo>> FindById(int id)
        {
            var response = await client.GetAsync($"{url}/findById/{id}");
            string strdata = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            List<BillInfoReponse> billInfos = System.Text.Json.JsonSerializer.Deserialize<List<BillInfoReponse>>(strdata, options);
            return mapper.Map<List<BillInfo>>(billInfos);
        }
    }
}
