﻿using OiHouseCoffee.Business_Layer.Models;

namespace OiHouseCoffee.Request
{
    public class CheckoutRequest
    {
        public BillRequest Bill { get; set; }

        public List<ProductCartRequest> ListProduct { get;set;}

        public int totalCheckout { get; set; }

    }
}
