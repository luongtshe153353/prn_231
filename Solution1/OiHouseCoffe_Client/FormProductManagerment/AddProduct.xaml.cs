﻿
using Microsoft.Win32;

using System;

using System.Diagnostics;

using System.Text.RegularExpressions;

using System.Windows;
using System.Windows.Controls;

using System.Windows.Media;

using Path = System.IO.Path;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffe_Client.Logic;
using Azure;
using OiHouseCoffee.Response;
using OiHouseCoffee.Request;
using System.Collections.Generic;
using AutoMapper;
using OiHouseCoffee.Mapping;

namespace OiHouseCoffee
{
    /// <summary>
    /// Interaction logic for AddProduct.xaml
    /// </summary>
    public partial class AddProduct : Window
    {
        

        private ListView listView;
        private Food? product;
        private static readonly Regex _regex = new Regex("[^0-9.-]+");
        private ProductManagement productManagement = new ProductManagement();
        private CategoryManagement categoryManagement = new CategoryManagement();
        private Dictionary<int, FoodCategory> mapFoodCategory = new Dictionary<int, FoodCategory>();
        IMapper mapper;

        public AddProduct(Food? _product, ListView _listView)
        {
            InitializeComponent();
            this.product = _product;
            this.listView = _listView;
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ProductMapping());
            });
            mapper = config.CreateMapper();
        }

        private async void Set_ItemComboBox()
        {
            var list = await categoryManagement.Search();
            foreach (var item in list)
            {
                mapFoodCategory.Add(item.Id, item);
            }
            comboBoxCategory.SelectedIndex = 0;
            ComboBoxItem comboBoxItemSelected = (ComboBoxItem)comboBoxCategory.SelectedItem;
            foreach (FoodCategory category in list)
            {
                comboBoxCategory.Items.Add(category);
            }
            comboBoxCategory.DisplayMemberPath = "Name";
            comboBoxCategory.SelectedValuePath = "Id";

            comboBoxCategory.Text = comboBoxItemSelected.Content.ToString();

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Set_ItemComboBox();
            if (product != null)
            {
                txtBoxName.Text = product.Name;
                txtBoxPrice.Text = product.Price.ToString();
                comboBoxCategory.SelectedValue = product.IdCategory;
                if (product.Image == null)
                {
                    ImageSourceConverter imgsc = new ImageSourceConverter();
                    string path = @"Images\Logo.JPG";
                    ImageSource imageSource = (ImageSource)imgsc.ConvertFromString(path);
                }
                else
                {
                    ImageSourceConverter imgsc = new ImageSourceConverter();
                    string path = @"Images\" + product.Image;
                    ImageSource imageSource = (ImageSource)imgsc.ConvertFromString(path);
                }

                txtBoxId.Visibility = Visibility.Visible;
                labelId.Visibility = Visibility.Visible;
            }
        }

        private async void btn_Click(object sender, RoutedEventArgs e)
        {
            string name = txtBoxName.Text;
            string price = txtBoxPrice.Text;
            if (comboBoxCategory.SelectedValue == null)
            {
                MessageBox.Show("Vui lòng chọn category!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if(String.IsNullOrEmpty(name))
            {
                MessageBox.Show("Vui lòng nhập tên sảm phẩm!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;

            }
            if (String.IsNullOrEmpty(name))
            {
                MessageBox.Show("Vui lòng nhập giá sản phẩm!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;

            }
            string categoryId = comboBoxCategory.SelectedValue.ToString();
            string avatarPath;
            if (imageProduct.Tag != null)
            {
                avatarPath = Path.GetFullPath(imageProduct.Tag.ToString());
            }
            else
            {
                avatarPath = @"Images\Logo.JPG";
            }
            ProductRequest? p = new ProductRequest();
            if (product != null)
            {
                p.Id = product.Id;
            }
            p.Name = name;
            p.Price = int.Parse(price);
            p.IdCategory = int.Parse(categoryId);
            p.Image = avatarPath;
            p.Status = true;
            ResponseCommon reponse;


            if (product != null)
            {
                reponse = await productManagement.UpdateProduct(p);
            }
            else
            {
                reponse = await productManagement.AddProduct(p);
            }
            if (!String.IsNullOrEmpty(reponse.msg))
            {
                MessageBox.Show(reponse.msg);
            }
            else
            {
                ProductSearchRequest productSearchRequest = new ProductSearchRequest();
                loadData(productSearchRequest);
                this.Close();
            }



        }

        public async void loadData(ProductSearchRequest productSearchRequest)
        {
            var list = await productManagement.Search(productSearchRequest);
            setCategory(list);
            listView.ItemsSource = list;
        }

        private void setCategory(List<Food> foodList)
        {
            foreach (Food food in foodList)
                food.IdCategoryNavigation = mapFoodCategory[food.IdCategory];
        }
        private void btn_Click_1(object sender, RoutedEventArgs e)
        {
            //Create a new instance of openFileDialog
            OpenFileDialog image = new OpenFileDialog();
            //Filter
            image.Filter = "Image Files|*.jpg;*.jpeg;*.png;*.tif;...";

            //When the user select the file
            if (image.ShowDialog() == true)
            {
                //Get the file's path
                string imgPath = image.FileName;
                //Do something
                ImageSourceConverter imgsc = new ImageSourceConverter();
                ImageSource imageSource = (ImageSource)imgsc.ConvertFromString(imgPath);
                imageProduct.Tag = imgPath;
                imageProduct.Source = imageSource;
            }
        }


        private void txtBoxPrice_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (_regex.IsMatch(txtBoxPrice.Text))
            {
                MessageBox.Show("Plase input number");
                txtBoxPrice.Text = string.Empty;
            }
        }

        
    }
}

