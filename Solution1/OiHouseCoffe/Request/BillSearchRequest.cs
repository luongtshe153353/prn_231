﻿namespace OiHouseCoffee.Request
{
    public class BillSearchRequest
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
