﻿namespace OiHouseCoffee.Request
{
    public class EmployeeSearchRequest
    {
        public int? AccountId { get; set; }
        public string? Email { get; set; }

        public string? UserName { get; set; }

        public string? PhoneNumber { get; set; }
    }
}
