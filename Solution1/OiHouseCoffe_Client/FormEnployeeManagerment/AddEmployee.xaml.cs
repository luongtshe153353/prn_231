﻿using Microsoft.Win32;
using OiHouseCoffe_Client.Logic;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Data_Layer.Repository;
using OiHouseCoffee.Request;
using OiHouseCoffee.Response;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Path = System.IO.Path;

namespace OiHouseCoffee
{
    /// <summary>
    /// Interaction logic for AddEmployee.xaml
    /// </summary>
    public partial class AddEmployee : Window
    {
        private readonly EmployeeManagement employeeManagement = new EmployeeManagement();
        private EmployeeManeger employeeManager;
        private Account? account;
        private ListView listView;
        private static readonly Regex _regex = new Regex("[^0-9.-]+");
        public AddEmployee(EmployeeManeger _employeeManeger, Account? _account, ListView _listView)
        {
            InitializeComponent();
            this.employeeManager = _employeeManeger;
            this.account = _account;
            this.listView = _listView;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (account != null)
            {
                txtBoxEmail.Text = account.Email;
                txtBoxUserName.Text = account.UserName;
                txtBoxPassWord.Text = account.PassWord;
                txtBoxPhoneNumber.Text = account.PassWord;
                txtBoxId.Text = account.Id.ToString();
                txtBoxId.Visibility = Visibility.Visible;
                labelId.Visibility = Visibility.Visible;
                btn.Content = "Update";
                this.Height = 550;
            }
        }
        private async void btn_Click(object sender, RoutedEventArgs e)
        {
            string email = txtBoxEmail.Text;
            string userName = txtBoxUserName.Text;
            string phoneNumber = txtBoxPhoneNumber.Text;
            string password = txtBoxPassWord.Text;
            string avatarPath;
            if (imageProduct.Tag != null)
            {
                avatarPath = Path.GetFullPath(imageProduct.Tag.ToString());
            }
            else
            {
                avatarPath = @"Images\Logo.JPG";
            }
            String msg = null;
            Account? p = null;
            if (account != null)
            {
                p = account;
            }
            else
            {
                p = new Account();
            }
            p.Email = email;
            p.UserName = userName;
            p.PhoneNumber = phoneNumber;
            p.PassWord = password;
            p.Avatar = avatarPath;
            p.Status = true;
            ResponseCommon reponse;
            if(account != null)
            {
               reponse = await employeeManagement.UpdateEmployee(p);
            }
            else
            {
                reponse = await employeeManagement.AddEmployee(p);
            }
            if(!String.IsNullOrEmpty(reponse.msg)) 
            {
                MessageBox.Show(reponse.msg);
            }
            else
            {
                this.Close();
                loadData();
            }
        }

        public async void loadData()
        {
            EmployeeSearchRequest employeeSearchRequest = new EmployeeSearchRequest();
            var list = await employeeManagement.Search(employeeSearchRequest);
            listView.ItemsSource = list;
        }
        private void txtBoxPhoneNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (_regex.IsMatch(txtBoxPhoneNumber.Text))
            {
                MessageBox.Show("Please input number");
                txtBoxPhoneNumber.Text = string.Empty;
            }
        }

        private void btn_Click1(object sender, RoutedEventArgs e)
        {
            //Create a new instance of openFileDialog
            OpenFileDialog image = new OpenFileDialog();
            //Filter
            image.Filter = "Image Files|*.jpg;*.jpeg;*.png;*.tif;...";

            //When the user select the file
            if (image.ShowDialog() == true)
            {
                //Get the file's path
                string imgPath = image.FileName;
                //Do something
                ImageSourceConverter imgsc = new ImageSourceConverter();
                ImageSource imageSource = (ImageSource)imgsc.ConvertFromString(imgPath);
                imageProduct.Tag = imgPath;
                imageProduct.Source = imageSource;
            }
        }
    }
}
