﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Data_Layer.Repository;
using OiHouseCoffee.Request;
using OiHouseCoffee.Response;

namespace OiHouseCoffe.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private QuanLyQuanCafeContext _context = new QuanLyQuanCafeContext();


        [HttpPost("signin")]
        public IActionResult Login([FromBody] AccountRequest account)
        {
            try
            {
                try
                {
                    var acc = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("account");
                    Account a = _context.Accounts.Where(x => x.UserName == account.UserName && x.PassWord == account.PassWord && x.Status == true).FirstOrDefault();
                    String username = account.UserName;
                    String password = account.PassWord;
                    if (username.Equals(acc["username"]) && password.Equals(acc["password"]))
                    {
                        return Ok(acc);
                    }
                    else if (a != null)
                    {
                        return Ok(a);
                    }
                    else
                    {
                        return Ok(null);
                    }
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
