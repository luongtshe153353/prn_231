﻿namespace OiHouseCoffee.Request
{
    public class BillRequest
    {
        public int Id { get; set; }

        public DateTime DateCheckIn { get; set; }

        public DateTime? DateCheckOut { get; set; }

        public int total { get; set; }

        public int? Discount { get; set; }

        public int? IdAccount { get; set; }
    }
}
