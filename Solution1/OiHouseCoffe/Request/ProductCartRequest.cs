﻿using OiHouseCoffee.Business_Layer.Models;

namespace OiHouseCoffee.Request
{
    public class ProductCartRequest
    {
        public ProductRequest product { get; set; }

        public int quantity { get; set; }

        public int price { get; set; }


        public void setPrice()
        {
            this.price = this.product.Price * this.quantity;
        }

        public void setPrice(int quantity)
        {
            this.quantity = quantity;
            this.price = this.product.Price * quantity;
        }

        public ProductCartRequest()
        {
        }

        public ProductCartRequest(ProductRequest food, int quantity)
        {
            this.product = food;
            this.quantity = quantity;
            this.price = product.Price * quantity;
        }

        
    }
}
