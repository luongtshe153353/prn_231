﻿using OiHouseCoffee.Business_Layer.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.Json;
using OiHouseCoffee.Response;
using OiHouseCoffee.Request;

namespace OiHouseCoffee
{
    /// <summary>
    /// Interaction logic for BillDetail.xaml
    /// </summary>
    public partial class BillDetail : Window
    {
        private List<ProductCartRequest> productCarts;
        private Account account;
        private int totalCheckout;
        private int discount;
        private int totalBeforDiscount;
        private readonly Home home;
        private long billId;
        private CheckoutResponse checkoutResponse;
        public BillDetail(List<ProductCartRequest> _productCarts,Account _account,int _totalCheckout,int _discount, int _totalBeforDiscount, Home _home, long _billId, CheckoutResponse _checkoutResponse)
        {
            InitializeComponent();
            productCarts = _productCarts;
            account = _account;
            discount = _discount;
            totalBeforDiscount = _totalBeforDiscount;
            totalCheckout = _totalCheckout;
            billId = _billId;
            checkoutResponse = _checkoutResponse;
            ListProduct.Items.Clear();
            ListProduct.ItemsSource = productCarts;
            setHeight();
            setInfo();
            this.home = _home;
            Closing += BillDetail_Closing;
            checkoutResponse = _checkoutResponse;
        }

        public void setHeight()
        {
            double desiredHeight = productCarts.Count*30;
            ListProduct.Height = desiredHeight;
            Height = desiredHeight + 720;
        }

        public void setInfo()
        {
            txtAccount.Text = account.UserName;
            timeCheckIn.Text = DateTime.Now.ToString();
            timeCheckOut.Text = DateTime.Now.ToString();
            TotalBeforDiscount.Text = totalBeforDiscount.ToString();
            PercentDiscount.Text = discount.ToString();
            TotalAfterDiscount.Text = totalCheckout.ToString();
            Discount.Text = (totalCheckout - totalBeforDiscount).ToString();
            vietQR.Source = ByteToImage(checkoutResponse.imageQR);

        }


        public static ImageSource ByteToImage(byte[] imageData)
        {
            BitmapImage biImg = new BitmapImage();
            MemoryStream ms = new MemoryStream(imageData);
            biImg.BeginInit();
            biImg.StreamSource = ms;
            biImg.EndInit();

            ImageSource imgSrc = biImg as ImageSource;

            return imgSrc;
        }

        private void BillDetail_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            home.reload();
        }

    }
}
