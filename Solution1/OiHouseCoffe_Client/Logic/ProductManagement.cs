﻿using Newtonsoft.Json;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using OiHouseCoffee.Response;
using AutoMapper;
using OiHouseCoffee.Mapping;

namespace OiHouseCoffe_Client.Logic
{
    internal class ProductManagement
    {

        private readonly HttpClient client = null;
        private string url = "";
        IMapper mapper;
        public ProductManagement()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ProductMapping());
            });
            mapper = config.CreateMapper();
            client = new HttpClient();
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);
            client.DefaultRequestHeaders.Accept.Clear();
            url = "http://localhost:5123/api/product";
        }

        public async Task<List<Food>> Search(ProductSearchRequest productSearchRequest)
        {
            var json = JsonConvert.SerializeObject(productSearchRequest);
            url = "http://localhost:5123/api/product/search";

            var content = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync($"{url}", content);

            string strdata = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            List<ResponseProduct> listProduct = System.Text.Json.JsonSerializer.Deserialize<List<ResponseProduct>>(strdata, options).ToList();
            return mapper.Map<List<Food>>(listProduct);
        }

        public async Task<ResponseCommon> AddProduct(ProductRequest productRequest)
        {
            var json = JsonConvert.SerializeObject(productRequest);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{url}/addProduct", content);
            string strdata = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            ResponseCommon responseCommon = System.Text.Json.JsonSerializer.Deserialize<ResponseCommon>(strdata, options);
            return responseCommon;

        }

        public async Task<ResponseCommon> UpdateProduct(ProductRequest productRequest)
        {
            var json = JsonConvert.SerializeObject(productRequest);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{url}/updateProduct", content);
            string strdata = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            ResponseCommon responseCommon = System.Text.Json.JsonSerializer.Deserialize<ResponseCommon>(strdata, options);
            return responseCommon;

        }

        public async Task<ResponseCommon> updateStatuFood(int id, bool status)
        {
            RequestUpdateStatus requestUpdateStatus = new RequestUpdateStatus();
            requestUpdateStatus.id = id;
            requestUpdateStatus.status = status;
            var json = JsonConvert.SerializeObject(requestUpdateStatus);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{url}/updateStatusFood", content);
            string strdata = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            ResponseCommon responseCommon = System.Text.Json.JsonSerializer.Deserialize<ResponseCommon>(strdata, options);
            return responseCommon;

        }

    }
}
