﻿using AutoMapper;
using Newtonsoft.Json;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Request;
using OiHouseCoffee.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace OiHouseCoffe_Client.Logic
{
    public class EmployeeManagement
    {

        private readonly HttpClient client = null;
        private string url = "";
        public EmployeeManagement()
        {
            client = new HttpClient();
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);
            client.DefaultRequestHeaders.Accept.Clear();
            url = "http://localhost:5123/api/employee";
        }

        public async Task<List<Account>> Search(EmployeeSearchRequest employeeSearchRequest)
        {
            var json = JsonConvert.SerializeObject(employeeSearchRequest);

            var content = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync($"{url}/search",content);

            string strdata = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            List<Account> listAccount = System.Text.Json.JsonSerializer.Deserialize<List<Account>>(strdata, options).ToList();
            return listAccount;
        }

        public async Task<ResponseCommon> AddEmployee(Account account)
        {
            var json = JsonConvert.SerializeObject(account);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{url}/addEmployee", content);
            string strdata = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            ResponseCommon responseCommon = System.Text.Json.JsonSerializer.Deserialize<ResponseCommon>(strdata, options);
            return responseCommon;

        }

        public async Task<ResponseCommon> UpdateEmployee(Account account)
        {
            var json = JsonConvert.SerializeObject(account);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{url}/updateEmployee", content);
            string strdata = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            ResponseCommon responseCommon = System.Text.Json.JsonSerializer.Deserialize<ResponseCommon>(strdata, options);
            return responseCommon;

        }

        public async Task<ResponseCommon> updateStatusEmployee(int id,bool status)
        {
            RequestUpdateStatus requestUpdateStatus = new RequestUpdateStatus();
            requestUpdateStatus.id = id;
            requestUpdateStatus.status = status;
            var json = JsonConvert.SerializeObject(requestUpdateStatus);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{url}/updateStatusAccount",content);
            string strdata = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            ResponseCommon responseCommon = System.Text.Json.JsonSerializer.Deserialize<ResponseCommon>(strdata, options);
            return responseCommon;

        }
    }
}
