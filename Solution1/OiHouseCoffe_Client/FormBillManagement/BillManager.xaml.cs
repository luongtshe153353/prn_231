﻿
using Microsoft.AspNetCore.Mvc.RazorPages.Infrastructure;
using OiHouseCoffe_Client.Logic;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

namespace OiHouseCoffee
{
    /// <summary>
    /// Interaction logic for BillManager.xaml
    /// </summary>
    public partial class BillManager : Page
    {

        private ProductManagement productManagement = new ProductManagement();

        private CategoryManagement categoryManagement = new CategoryManagement();

        private BillManagement billManagement = new BillManagement();

        private EmployeeManagement employeeManagement = new EmployeeManagement();

        private Dictionary<int, Account> mapAccount = new Dictionary<int, Account>();

        private static readonly Regex _regex = new Regex("[^0-9.-]+");
        public BillManager()
        {
            InitializeComponent();

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            BillSearchRequest billSearchRequest = new BillSearchRequest();

            loadData(billSearchRequest);
        }

        public async void loadData(BillSearchRequest billSearchRequest)
        {
            EmployeeSearchRequest employeeSearchRequest = new EmployeeSearchRequest();
            var list = await billManagement.Search(billSearchRequest);
            var listEmployee = await employeeManagement.Search(employeeSearchRequest);
            mapAccount.Clear();
            foreach (var item in listEmployee)
            {
                mapAccount.Add(item.Id, item);
            }
            foreach (var item in list)
            {
                item.IdAccountNavigation = mapAccount[item.IdAccount];
            }
            listView.ItemsSource = list;

        }
        private void ListViewItem_Selected(object sender, RoutedEventArgs e)
        {

        }
        private void ListView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ListView? listView = sender as ListView;
            GridView? gridView = listView != null ? listView.View as GridView : null;

            var width = listView != null ? listView.ActualWidth - SystemParameters.VerticalScrollBarWidth : this.Width;

            var column1 = 0.05;
            var column2 = 0.2;
            var column3 = 0.1;
            var column4 = 0.2;
            var column5 = 0.2;
            var column6 = 0.2;


            if (gridView != null && width >= 0)
            {
                gridView.Columns[0].Width = width * column1;
                gridView.Columns[1].Width = width * column2;
                gridView.Columns[2].Width = width * column3;
                gridView.Columns[3].Width = width * column4;
                gridView.Columns[4].Width = width * column5;
                gridView.Columns[5].Width = width * column6;

            }
        }

        private void btnReload_Click(object sender, RoutedEventArgs e)
        {
            BillSearchRequest billSearchRequest = new BillSearchRequest();
            loadData(billSearchRequest);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            DateTime? startDate = StartDate.SelectedDate == null ? null : StartDate.SelectedDate.Value.Date;
            DateTime? endDate = EndDate.SelectedDate == null ? null : EndDate.SelectedDate.Value.Date;
            BillSearchRequest billSearchRequest = new BillSearchRequest();
            billSearchRequest.StartDate = startDate;
            billSearchRequest.EndDate = endDate;
            loadData(billSearchRequest);
        }
        private void Get_Detail(object sender, RoutedEventArgs e)
        {
            Bill bill = listView.SelectedItems.Cast<Bill>().SingleOrDefault();
            if(bill == null)
            {
                MessageBox.Show("Chọn bill muốn xem");
            }
            else
            {
                BillDetailInfo billDetailInfo = new BillDetailInfo(bill);
                billDetailInfo.Show();
            }
            
        }
    }
}
