﻿
using OiHouseCoffe_Client.FormProductManagerment;
using OiHouseCoffe_Client.Logic;
using OiHouseCoffee.Data_Layer.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace OiHouseCoffee
{
    /// <summary>
    /// Interaction logic for AdminManager.xaml
    /// </summary>
    public partial class AdminManager : Window
    {
       
        public AdminManager()
        {
            InitializeComponent();
           
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            this.Visibility = Visibility.Collapsed;
            e.Cancel = true;
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
        }

        

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if(e.ChangedButton == MouseButton.Left)
            {
                this.DragMove();
            }
        }



        private void Button_Click(object sender, RoutedEventArgs e)
        {
            EmployeeManeger employeeManeger = new EmployeeManeger();
            frameMain.Content = employeeManeger;
            setButtonColor(0);
        }



        private void button_click_1(object sender, RoutedEventArgs e)
        {
            ProductManagerment productmanager = new ProductManagerment();
            frameMain.Content = productmanager;
            setButtonColor(1);
        }



        private void Button_Click_2(object sender, RoutedEventArgs e)
        {

            BillManager billManager = new BillManager();
            frameMain.Content = billManager;
            setButtonColor(2);
        }

        public void setButtonColor(int numberButton)
        {
            if (numberButton == 0)

            {
                buttonEmployee.Background = (SolidColorBrush)new BrushConverter().ConvertFrom("#02be68");
                buttonProduct.Background = null;
                buttonBill.Background = null;
                buttonEmployee.Foreground = new SolidColorBrush(Colors.White);
                buttonProduct.Foreground = (SolidColorBrush)new BrushConverter().ConvertFrom("#FFCFCFCF");
                buttonBill.Foreground = (SolidColorBrush)new BrushConverter().ConvertFrom("#FFCFCFCF");
            }
            else if (numberButton == 1)
            {
                buttonProduct.Background = (SolidColorBrush)new BrushConverter().ConvertFrom("#02be68");
                buttonEmployee.Background = null;
                buttonBill.Background = null;
                buttonProduct.Foreground = new SolidColorBrush(Colors.White);
                buttonEmployee.Foreground = (SolidColorBrush)new BrushConverter().ConvertFrom("#FFCFCFCF");
                buttonBill.Foreground = (SolidColorBrush)new BrushConverter().ConvertFrom("#FFCFCFCF");
            }
            else
            {
                buttonBill.Background = (SolidColorBrush)new BrushConverter().ConvertFrom("#02be68");
                buttonProduct.Background = null;
                buttonEmployee.Background = null;
                buttonBill.Foreground = new SolidColorBrush(Colors.White);
                buttonEmployee.Foreground = (SolidColorBrush)new BrushConverter().ConvertFrom("#FFCFCFCF");
                buttonProduct.Foreground = (SolidColorBrush)new BrushConverter().ConvertFrom("#FFCFCFCF");
            }
        }
    }
}
