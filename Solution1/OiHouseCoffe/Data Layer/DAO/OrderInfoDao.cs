﻿using OiHouseCoffee.Business_Layer.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OiHouseCoffee.Data_Layer
{
    public class OrderInfoDao
    {
        private static OrderInfoDao instance = null;
        private static readonly object instanceLock = new object();

        private OrderInfoDao()
        {
        }

        public static OrderInfoDao Instance
        {
            get
            {
                lock (instanceLock)
                {
                    if (instance == null)
                    {
                        instance = new OrderInfoDao();
                    }
                    return instance;
                }

            }
        }

        public void Add(BillInfo OrderInfo)
        {
            try
            {

                using (var saleManagement = new QuanLyQuanCafeContext())
                {
                    saleManagement.BillInfos.Add(OrderInfo);
                    saleManagement.SaveChanges();
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public IEnumerable<BillInfo> GetAllById(int id) {

            List<BillInfo> billInfos = new List<BillInfo>();
            try
            {
                using (var dbContext = new QuanLyQuanCafeContext())
                {
                    billInfos = dbContext.BillInfos.Include(x => x.IdFoodNavigation).Where(x => x.IdBillNavigation.Id == id).ToList();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return billInfos;
        }


    }
}
