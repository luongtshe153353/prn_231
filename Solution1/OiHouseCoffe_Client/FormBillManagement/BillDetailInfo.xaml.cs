﻿using OiHouseCoffe_Client.Logic;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Data_Layer.Repository;
using OiHouseCoffee.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OiHouseCoffee
{
    /// <summary>
    /// Interaction logic for BillDetailInfo.xaml
    /// </summary>
    public partial class BillDetailInfo : Window
    {
        private Bill bill;

        BillManagement billManagement = new BillManagement();

        ProductManagement productManagement = new ProductManagement();

        private Dictionary<int, Food> mapFood = new Dictionary<int, Food>();

        public BillDetailInfo(Bill _bill)
        {
            bill = _bill;
            InitializeComponent();
            setUp();
        }

        public async void setUp()
        {
            ProductSearchRequest productSearchRequest = new ProductSearchRequest();
            var billInfos = await billManagement.FindById(bill.Id);
            var listProduct = await productManagement.Search(productSearchRequest);
            txtAccount.Text = bill.IdAccountNavigation.UserName;
            timeCheckIn.Text = DateTime.Now.ToString();
            timeCheckOut.Text = DateTime.Now.ToString();
            int totalBeforDiscount = 0;
            int discount = 0;
            int totalAfterDiscount = 0;
            foreach(Food food in listProduct)
            {
                mapFood.Add(food.Id, food);
            }
            foreach (BillInfo billinfo in billInfos)
            {
                billinfo.IdFoodNavigation = mapFood[billinfo.IdFood];
                billinfo.price = billinfo.Count * billinfo.IdFoodNavigation.Price;
                totalBeforDiscount += billinfo.Count * billinfo.IdFoodNavigation.Price;
            }
            discount = (int)bill.Discount;
            TotalBeforDiscount.Text = totalBeforDiscount.ToString();
            if(discount > 0)
            {
                totalAfterDiscount = totalBeforDiscount * discount / 100;
            }
            else
            {
                totalAfterDiscount = totalBeforDiscount;
            }
           
            PercentDiscount.Text = discount.ToString();
            TotalAfterDiscount.Text = totalAfterDiscount.ToString();
            Discount.Text = (totalAfterDiscount - totalBeforDiscount).ToString();
            ListProduct.ItemsSource = billInfos;
        }
    }
}
