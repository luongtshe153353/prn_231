﻿using OiHouseCoffe_Client.Logic;
using OiHouseCoffee;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

using System.Windows.Media;


namespace OiHouseCoffe_Client.FormProductManagerment
{
    /// <summary>
    /// Interaction logic for ProductManagerment.xaml
    /// </summary>
    public partial class ProductManagerment : Page
    {
        private ProductManagement productManagement = new ProductManagement() ;

        private CategoryManagement categoryManagement = new CategoryManagement() ;

        private static readonly Regex _regex = new Regex("[^0-9.-]+");

        private Dictionary<int,FoodCategory> mapFoodCategory = new Dictionary<int,FoodCategory>();
        public ProductManagerment()
        {
            InitializeComponent();
            this.listView.SelectionChanged += ListView_SelectionChanged;
        }


        private void ListView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ListView? listView = sender as ListView;
            GridView? gridView = listView != null ? listView.View as GridView : null;

            var width = listView != null ? listView.ActualWidth - SystemParameters.VerticalScrollBarWidth : this.Width;

            var column1 = 0.2;
            var column2 = 0.2;
            var column3 = 0.2;
            var column4 = 0.2;
            var column5 = 0.2;

            if (gridView != null && width >= 0)
            {
                gridView.Columns[0].Width = width * column1;
                gridView.Columns[1].Width = width * column2;
                gridView.Columns[2].Width = width * column3;
                gridView.Columns[3].Width = width * column4;
                gridView.Columns[4].Width = width * column5;

            }
        }



        private async void Set_ItemComboBox()
        {
            var list = await categoryManagement.Search();
            foreach(var item in list)
            {
                mapFoodCategory.Add(item.Id, item);
            }
            FoodCategory foodCategory = new FoodCategory();
            foodCategory.Name = "All category";
            foodCategory.Id = 0;
            list.Add(foodCategory);
            comboBoxCategory.ItemsSource = list;
            comboBoxCategory.DisplayMemberPath = "Name";
            comboBoxCategory.SelectedValuePath = "Id";
            comboBoxCategory.SelectedIndex = list.Count - 1;
        }



        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            ProductSearchRequest productSearchRequest = new ProductSearchRequest();
            Set_ItemComboBox();
            loadData(productSearchRequest);
        }

        public async void loadData(ProductSearchRequest productSearchRequest)
        {            
            var list = await productManagement.Search(productSearchRequest);
            setCategory(list);
            listView.ItemsSource = list;
        }
        private void Open_Add(object sender, RoutedEventArgs e)
        {
            AddProduct addProduct = new AddProduct(null, listView);
            addProduct.Show();
        }


        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int count = listView.SelectedItems.Count;
            if (count > 0)
            {
                btnEdit.IsEnabled = true;
            }
            else
            {
                btnEdit.IsEnabled = false;

                btnEdit.Background = new SolidColorBrush(Colors.Gray);
            }
        }

        private void comboBoxCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            

        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            //MessageBoxResult messageBoxResult = MessageBox.Show("Do you wan't remove Product?", "Remove product", MessageBoxButton.YesNo);
            //if (messageBoxResult == MessageBoxResult.Yes)
            //{
            //    List<Food> foods = listView.SelectedItems.Cast<Food>().ToList();
            //    foods.ForEach(foods => productRepository.Remove(foods));
            //    listView.ItemsSource = productRepository.List();
            //}
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            int count = listView.SelectedItems.Count;
            if (count > 0)
            {
                List<Food> products = listView.SelectedItems.Cast<Food>().ToList();
                products.ForEach(product =>
                {
                    AddProduct addProduct = new AddProduct( product, listView);
                    addProduct.Show();
                });
            }
            else
            {
                MessageBox.Show("Plase select member");
            }
        }

        private void btnReload_Click(object sender, RoutedEventArgs e)
        {   ProductSearchRequest request = new ProductSearchRequest();
            loadData(request);
            searchById.Text = null;
            searchByPrice.Text = null;
            comboBoxCategory.SelectedIndex = comboBoxCategory.Items.Count - 1;
        }

        private void setCategory(List<Food> foodList)
        {
            foreach (Food food in foodList)
                food.IdCategoryNavigation = mapFoodCategory[food.IdCategory];
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            int? id = !String.IsNullOrEmpty(searchById.Text) ? int.Parse(searchById.Text) : null;
            string? name = searchByName.Text;
            int? price = !String.IsNullOrEmpty(searchByPrice.Text) ? int.Parse(searchByPrice.Text) : null;
            bool idNotNull = comboBoxCategory.SelectedValue != null && !comboBoxCategory.SelectedValue.Equals(0);
            String idCategory = idNotNull ? comboBoxCategory.SelectedValue.ToString() : null;
            ProductSearchRequest productFilter = new ProductSearchRequest();
            productFilter.Id = id;
            productFilter.ProductName = name;
            productFilter.Price = price;
            productFilter.CategoryId = !String.IsNullOrEmpty(idCategory) ? int.Parse(idCategory) : null;
            loadData(productFilter);
        }

        private void Button_Add(object sender, RoutedEventArgs e)
        {

        }

        private void searchById_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (_regex.IsMatch(searchById.Text))
            {
                MessageBox.Show("Please input number");
                searchById.Text = string.Empty;
            }
        }


        private void searchByPrice_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (_regex.IsMatch(searchByPrice.Text))
            {
                MessageBox.Show("Please input number");
                searchByPrice.Text = string.Empty;
            }
        }

        private void ListViewItem_Selected(object sender, RoutedEventArgs e)
        {

        }
    }
}

