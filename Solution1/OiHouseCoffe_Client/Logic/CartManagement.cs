﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OiHouseCoffee.Request;
using OiHouseCoffee.Response;
using System.Text.Json;

namespace OiHouseCoffe_Client.Logic
{
    internal class CartManagement
    {
        private readonly HttpClient client = null;
        private string url = "";
        public CartManagement()
        {
            
            client = new HttpClient();
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);
            client.DefaultRequestHeaders.Accept.Clear();
            url = "http://localhost:5123/api/cart";
        }


        public async Task<CheckoutResponse> CheckOut(CheckoutRequest checkoutRequest)
        {
            var json = JsonConvert.SerializeObject(checkoutRequest);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync($"{url}/checkout", content);
            string strdata = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            CheckoutResponse checkoutResponse = System.Text.Json.JsonSerializer.Deserialize<CheckoutResponse>(strdata, options);
            return checkoutResponse;

        }
    }


}
