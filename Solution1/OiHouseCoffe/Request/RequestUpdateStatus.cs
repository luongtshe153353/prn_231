﻿namespace OiHouseCoffee.Request
{
    public class RequestUpdateStatus
    {
        public int id { get; set; }
        public bool status { get; set; }
    }
}
