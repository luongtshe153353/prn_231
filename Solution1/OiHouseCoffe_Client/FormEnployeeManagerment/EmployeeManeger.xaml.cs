﻿using Azure;
using OiHouseCoffe_Client.Logic;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Data_Layer.Repository;
using OiHouseCoffee.Request;
using OiHouseCoffee.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;


namespace OiHouseCoffee
{
   
    public partial class EmployeeManeger : Page
    {
        private static readonly Regex _regex = new Regex("[^0-9.-]+");
        private EmployeeManagement employeeManegement = new EmployeeManagement();
        public EmployeeManeger()
        {
            InitializeComponent();
            this.listView.SelectionChanged += ListView_SelectionChanged;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            loadData();
        }

        public async void loadData()
        {
            EmployeeSearchRequest employeeSearchRequest = new EmployeeSearchRequest();

            var list  = await employeeManegement.Search(employeeSearchRequest);
            listView.ItemsSource = list;
        }

        

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int count = listView.SelectedItems.Count;
            if (count > 0)
            {
                btnEdit.IsEnabled = true;
            }
            else
            {
                btnEdit.IsEnabled = false;
                btnEdit.Background = new SolidColorBrush(Colors.Gray);
            }
        }
        private void ListViewItem_Selected(object sender, RoutedEventArgs e)
        {

        }
        private void ListView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ListView? listView = sender as ListView;
            GridView? gridView = listView != null ? listView.View as GridView : null;

            var width = listView != null ? listView.ActualWidth - SystemParameters.VerticalScrollBarWidth : this.Width;

            var column1 = 0.1;
            var column2 = 0.2;
            var column3 = 0.2;
            var column4 = 0.2;
            var column5 = 0.2;
            var column6 = 0.2;


            if (gridView != null && width >= 0)
            {
                gridView.Columns[0].Width = width * column1;
                gridView.Columns[1].Width = width * column2;
                gridView.Columns[2].Width = width * column3;
                gridView.Columns[3].Width = width * column4;
                gridView.Columns[4].Width = width * column5;
                gridView.Columns[5].Width = width * column6;

            }
        }
        

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int? id = !String.IsNullOrEmpty(searchById.Text) ? int.Parse(searchById.Text) : null;
            string email = searchByEmail.Text;
            string userName = searchByName.Text;
            string phoneNumber = searchByPhoneNumber.Text;
            EmployeeSearchRequest employeeFilter = new EmployeeSearchRequest();
            employeeFilter.AccountId = id;
            employeeFilter.Email = email;
            employeeFilter.PhoneNumber = phoneNumber;
            employeeFilter.UserName = userName;
            loadDataSearch(employeeFilter);

        }

        public async void loadDataSearch(EmployeeSearchRequest employeeSearchRequest)
        {
            var list = await employeeManegement.Search(employeeSearchRequest);
            listView.ItemsSource = list;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            AddEmployee addemployee = new AddEmployee(this, null,listView);
            addemployee.Show();
        }

        private void btnReload_Click(object sender, RoutedEventArgs e)
        {

            loadData();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            
        }

        
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            int count = listView.SelectedItems.Count;
            if (count > 0)
            {
                List<Account> accounts = listView.SelectedItems.Cast<Account>().ToList();
                accounts.ForEach(member =>
                {
                    AddEmployee addEmployee = new AddEmployee(this, member, listView);
                    addEmployee.Show();
                });
            }
            else
            {
                MessageBox.Show("Please select member");
            }
        }


        private void searchById_TextChanged_1(object sender, TextChangedEventArgs e)
        {
            if (_regex.IsMatch(searchById.Text))
            {
                MessageBox.Show("Plase input number");
                searchById.Text = string.Empty;
            }
        }

        private void searchByPhoneNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (_regex.IsMatch(searchByPhoneNumber.Text))
            {
                MessageBox.Show("Please input number");
                searchById.Text = string.Empty;
            }
        }

        private void searchByEmail_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void btnUpdateStatus_Click(object sender, RoutedEventArgs e)
        {
            if(listView.SelectedItem == null)
            {
                MessageBox.Show("Please select account!");
            }
            else
            {
                Account member = listView.SelectedItems.Cast<Account>().FirstOrDefault();
                MessageBoxResult messageboxresult;
                if (member.Status == true)
                {
                    messageboxresult = MessageBox.Show("do you wan't deactive account?", "remove account", MessageBoxButton.YesNo);
                }
                else
                {
                    messageboxresult = MessageBox.Show("do you wan't active account?", "remove account", MessageBoxButton.YesNo);
                }
               
                if (messageboxresult == MessageBoxResult.Yes)
                {
                    if (member.Status == true)
                    {
                        updateStatusUser(member.Id, false);
                    }
                    else
                    {
                        updateStatusUser(member.Id, true);
                    }
                }
                loadData();
            }
            
        }

        public async void updateStatusUser(int id, bool status)
        {
            ResponseCommon reponseCommon = new ResponseCommon();
            reponseCommon = await employeeManegement.updateStatusEmployee(id, status);
        }

        
    }
}
