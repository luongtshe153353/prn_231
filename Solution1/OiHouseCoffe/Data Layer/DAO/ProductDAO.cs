﻿
using OiHouseCoffee.Business_Layer.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using OiHouseCoffee.Response;

namespace OiHouseCoffee.Data_Layer
{
    public class ProductDao
    {
        private static ProductDao instance = null;
        private static readonly object instanceLock = new object();
        private ProductDao() { }
        public static ProductDao Instance
        {
            get
            {
                lock (instanceLock)
                {
                    if (instance == null)
                    {
                        instance = new ProductDao();
                    }
                    return instance;
                }

            }
        }

        public IEnumerable<Food> List()
        {
            List<Food> Foods = new List<Food>();
            try
            {
                using (var dbContext = new QuanLyQuanCafeContext())
                {
                    Foods = dbContext.Foods.Include(x => x.IdCategoryNavigation).ToList();
                } 
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return Foods;
        }

        public Food FindOne(Expression<Func<Food, bool>> predicate)
        {
            Food Food = null;
            try
            {
                using (var dbContext = new QuanLyQuanCafeContext())
                {
                    Food = dbContext.Foods.Include(x => x.IdCategoryNavigation).SingleOrDefault(predicate);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return Food;
        }

        public IEnumerable<Food> FindAll(Expression<Func<Food, bool>> predicate)
        {
            List<Food> Foods = new List<Food>();
            try
            {
                using (var dbContext = new QuanLyQuanCafeContext())
                {
                    Foods = dbContext.Foods.Include(x => x.IdCategoryNavigation).Where(predicate).ToList();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return Foods;
        }


        public void Add(Food food)
        {
            try
            {
                Food foodNew = FindOne(item => item.Id.Equals(food.Id));
                if (foodNew == null)
                {
                    using (var saleManagement = new QuanLyQuanCafeContext())
                    {
                        saleManagement.Foods.Add(food);
                        saleManagement.SaveChanges();
                    }

                }
                else
                {
                    throw new Exception("The Food is already exist");
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Delete(Food food)
        {
            try
            {
                Food foodNew = FindOne(item => item.Id.Equals(food.Id));
                if (foodNew != null)
                {
                    using (var dbContext = new QuanLyQuanCafeContext())
                    {
                        dbContext.Foods.Remove(foodNew);
                        dbContext.SaveChanges();
                    }
                }
                else
                {
                    throw new Exception("The Food does not exist");
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Update(Food food)
        {
            try
            {
                Food foodNew = FindOne(item => item.Id.Equals(food.Id));
                if (foodNew != null)
                {
                    using (var saleManager = new QuanLyQuanCafeContext())
                    {
                        saleManager.Entry<Food>(food).State = EntityState.Modified;
                        saleManager.SaveChanges();
                    }
                }
                else
                {
                    throw new Exception("The Food does not exist");
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void updateStatusFood(int Id, bool status)
        {
            try
            {
                Food food = FindOne(item => item.Id.Equals(Id));
                if (food != null)
                {
                    food.Status = status;
                    using (var saleManager = new QuanLyQuanCafeContext())
                    {
                        saleManager.Entry<Food>(food).State = EntityState.Modified;
                        saleManager.SaveChanges();
                    }
                }
                else
                {
                    throw new Exception("The Account does not exist");
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

    }


}
