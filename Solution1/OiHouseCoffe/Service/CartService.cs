﻿using AutoMapper;
using Microsoft.OpenApi.Models;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Data_Layer.Repository;
using OiHouseCoffee.Mapping;
using OiHouseCoffee.Request;
using OiHouseCoffee.Response;
using RestSharp;
using System.Text.Json;

namespace OiHouseCoffee.Service
{
    public class CartService
    {

        BillRepository billRepository = new BillRepository();

        OrderInfoRepository orderInfoRepository = new OrderInfoRepository();

        IMapper mapper = new MapperConfiguration(cfg =>
            {cfg.AddProfile(new BillMapping());}).CreateMapper();

        public CheckoutResponse checkout(CheckoutRequest checkoutRequest)
        {
            CheckoutResponse reponse = new CheckoutResponse();
            Bill bill = mapper.Map<Bill>(checkoutRequest.Bill);
            billRepository.Add(bill);

            foreach (ProductCartRequest item in checkoutRequest.ListProduct)
            {
                BillInfo billInfo = new BillInfo();
                billInfo.IdBill = bill.Id;
                billInfo.IdFood = item.product.Id;
                billInfo.Count = item.quantity;
                billInfo.price = item.price;
                orderInfoRepository.Add(billInfo);
            }
            reponse.bill = bill;
            reponse.imageQR = createQr(bill.Id, checkoutRequest.totalCheckout);
            return reponse;
        }

        public byte[] createQr(int billId, int totalCheckout)
        {
            var apiRequest = new ApiRequest();
            apiRequest.acqId = 970415;
            apiRequest.accountNo = 104872819334;
            apiRequest.accountName = "Tong Sy Luong";
            apiRequest.amount = Convert.ToInt32(totalCheckout.ToString());
            apiRequest.format = "text";
            apiRequest.template = "qr_only";
            apiRequest.addInfo = "Ma hoa don: " + billId + ". Cam on quy khach.";
            var jsonRequest = JsonSerializer.Serialize<ApiRequest>(apiRequest);
            // use restsharp for request api.
            var client = new RestClient("https://api.vietqr.io/v2/generate");
            var request = new RestRequest();

            request.Method = Method.Post;
            request.AddHeader("Accept", "application/json");

            request.AddParameter("application/json", jsonRequest, ParameterType.RequestBody);

            var response = client.Execute(request);
            var content = response.Content;
            var dataResult = JsonSerializer.Deserialize<ApiResponse>(content);

            string base64String = dataResult.data.qrDataURL.Replace("data:image/png;base64,", "");
            byte[] imageBytes = Convert.FromBase64String(base64String);
            return imageBytes;
        }

        
    }


}
