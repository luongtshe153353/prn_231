﻿using OiHouseCoffee.Business_Layer.Models;

namespace OiHouseCoffee.Response
{
    public class ResponseProduct
    {
        public int Id { get; set; }

        public string Name { get; set; } = null!;

        public int IdCategory { get; set; }

        public int Price { get; set; }

        public string? Image { get; set; }


        public bool? Status { get; set; }

    }
}
