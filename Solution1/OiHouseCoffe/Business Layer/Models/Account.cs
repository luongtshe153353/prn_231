﻿using System;
using System.Collections.Generic;

namespace OiHouseCoffee.Business_Layer.Models;

public partial class Account
{
    public string UserName { get; set; } = null!;

    public string Email { get; set; } = null!;

    public string PassWord { get; set; } = null!;


    public int Id { get; set; }

    public string? Avatar { get; set; }

    public string? PhoneNumber { get; set; }

    public bool? Status { get; set; }

    public virtual ICollection<Bill> Bills { get; set; } = new List<Bill>();
}
