﻿using AutoMapper;
using OiHouseCoffe_Client.Logic;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Mapping;
using OiHouseCoffee.Request;
using OiHouseCoffee.Response;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OiHouseCoffee
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : Window
    {

        private Account account;
        private static readonly Regex _regex = new Regex("[^0-9.-]+");
        List<ProductCartRequest> listProductCart = new List<ProductCartRequest>();

        private ProductManagement productManagement = new ProductManagement();

        private CategoryManagement categoryManagement = new CategoryManagement();

        private CartManagement cartManagement = new CartManagement();

        private Dictionary<int, FoodCategory> mapFoodCategory = new Dictionary<int, FoodCategory>();

        private IMapper _mapper;

        public Home(Account _account)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ProductMapping());
            });
            _mapper = config.CreateMapper();
            InitializeComponent();
            ListCart.ItemsSource = listProductCart;
            account = _account;
            setTotal();
            setTotalCheckOut();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            Set_ItemComboBox();
            ProductSearchRequest productSearchRequest = new ProductSearchRequest();
            loadData(productSearchRequest);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            this.Visibility = Visibility.Collapsed;
            e.Cancel = true;
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
        }

        public async void loadData(ProductSearchRequest productSearchRequest)
        {
            var list = await productManagement.Search(productSearchRequest);
            if(mapFoodCategory.Count == 0) {
                Set_ItemComboBox();
            }
            setCategory(list);
            ListProduct.ItemsSource = list;
            
        }

        private void setCategory(List<Food> foodList)
        {
                
                foreach (Food food in foodList)
                    food.IdCategoryNavigation = mapFoodCategory[food.IdCategory];
            
        }

        private async void Set_ItemComboBox()
        {
            mapFoodCategory.Clear();
            var list = await categoryManagement.Search();
            foreach (var item in list)
            {
                mapFoodCategory.Add(item.Id, item);
            }
            FoodCategory foodCategory = new FoodCategory();
            foodCategory.Name = "All category";
            foodCategory.Id = 0;
            list.Add(foodCategory);
            comboBoxCategory.ItemsSource = list;
            comboBoxCategory.DisplayMemberPath = "Name";
            comboBoxCategory.SelectedValuePath = "Id";
            comboBoxCategory.SelectedIndex = list.Count - 1;

        }


        private void Home_Closing(object sender, CancelEventArgs e)
        {
            this.Visibility = Visibility.Visible;
            this.Close();
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();

        }

        private void comboBoxCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ProductSearchRequest productFilter = new ProductSearchRequest();
            String idCategory;
            if (comboBoxCategory.SelectedValue != null)
            {
                idCategory = comboBoxCategory.SelectedValue.ToString();
                if (idCategory.Equals("0"))
                {
                    idCategory = null;
                }
            }
            else
            {
                idCategory = null;
            }

                productFilter.ProductName = searchByName.Text;
                productFilter.CategoryId = !String.IsNullOrEmpty(idCategory) ? int.Parse(idCategory) : null;
                loadData(productFilter);
            
        }
        private void searchByName_TextChanged(object sender, TextChangedEventArgs e)
        {
            ProductSearchRequest productFilter = new ProductSearchRequest();
            String idCategory;
            if (comboBoxCategory.SelectedValue != null)
            {
                idCategory = comboBoxCategory.SelectedValue.ToString();
                if (idCategory.Equals("0"))
                {
                    idCategory = null;
                }
            }
            else
            {
                idCategory = null;
            }
            productFilter.ProductName = searchByName.Text;
            productFilter.CategoryId = !String.IsNullOrEmpty(idCategory) ? int.Parse(idCategory) : null;
            loadData(productFilter);
        }

        private void Control_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListView lv = sender as ListView;
            Food food = lv.SelectedItems.Cast<Food>().FirstOrDefault();
            ProductRequest productRequest = _mapper.Map<ProductRequest>(food);
            ProductCartRequest item = new ProductCartRequest(productRequest, 1);
            int index = listProductCart.FindIndex(x => x.product.Id == item.product.Id);
            if (index >= 0)
            {
                listProductCart[index].quantity++;
                listProductCart[index].setPrice();
            }
            else
            {
                listProductCart.Add(_mapper.Map<ProductCartRequest>(item));
            }
            ListCart.ItemsSource = new ObservableCollection<ProductCartRequest>(listProductCart);
            CollectionViewSource.GetDefaultView(ListCart.ItemsSource).Refresh();
            setTotal();
            setTotalCheckOut();

        }

        private void Button_Add_Click(object sender, RoutedEventArgs e)
        {
            ProductCartRequest item = ListCart.SelectedItems.Cast<ProductCartRequest>().FirstOrDefault();
            int index = listProductCart.FindIndex(x => x.product.Id == item.product.Id);
            listProductCart[index].quantity++;
            listProductCart[index].setPrice();
            ListCart.SelectedItem = item;
            ListCart.ItemsSource = new ObservableCollection<ProductCartRequest>(listProductCart);
            CollectionViewSource.GetDefaultView(ListCart.ItemsSource).Refresh();
            setTotal();
            setTotalCheckOut();
        }
        private void Button_Remove_Click(object sender, RoutedEventArgs e)
        {
            ProductCartRequest item = ListCart.SelectedItems.Cast<ProductCartRequest>().FirstOrDefault();
            int index = listProductCart.FindIndex(x => x.product.Id == item.product.Id);
            listProductCart[index].quantity--;
            if(listProductCart[index].quantity == 0)
            {
                listProductCart.RemoveAt(index);
                ListCart.ItemsSource = new ObservableCollection<ProductCartRequest>(listProductCart);
                CollectionViewSource.GetDefaultView(ListCart.ItemsSource).Refresh();
            }
            else
            {
                listProductCart[index].setPrice();
                ListCart.SelectedItem = item;
                ListCart.ItemsSource = new ObservableCollection<ProductCartRequest>(listProductCart);
                CollectionViewSource.GetDefaultView(ListCart.ItemsSource).Refresh();
            }
            setTotal();
            setTotalCheckOut();
        }

        private void Quantity_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBoxQuantity = sender as TextBox;
            if(!String.IsNullOrEmpty(textBoxQuantity.Text))
            {
                int quantity = int.Parse(textBoxQuantity.Text);
                ProductCartRequest item = ListCart.SelectedItems.Cast<ProductCartRequest>().FirstOrDefault();
                int index = listProductCart.FindIndex(x => x.product.Id == item.product.Id);
                listProductCart[index].setPrice(quantity);
                ListCart.SelectedItem = item;
                ListCart.ItemsSource = new ObservableCollection<ProductCartRequest>(listProductCart);
                CollectionViewSource.GetDefaultView(ListCart.ItemsSource).Refresh();
                setTotal();
                setTotalCheckOut();
            }
            else
            {
                
                MessageBox.Show("Nếu bạn muốn xóa sản phẩm khỏi giỏ hàng hãy nhập số 0. Hoặc số sản phẩm bạn muốn lớn hơn 0.");
                ProductCartRequest item = ListCart.SelectedItems.Cast<ProductCartRequest>().FirstOrDefault();
                int index = listProductCart.FindIndex(x => x.product.Id == item.product.Id);
                textBoxQuantity.Text = listProductCart[index].quantity.ToString();
            }
            
        }

        public void setTotal()
        {
            int total = 0;
            foreach(var item in listProductCart)
            {
                total+= item.quantity * item.product.Price;
            }
            TotalBeforeDiscount.Content = total.ToString();
        }

        public void setTotalCheckOut()
        {
            int total = int.Parse(TotalBeforeDiscount.Content.ToString());
            if (!String.IsNullOrEmpty(Discount.Text))
            {
                int discount = int.Parse(Discount.Text);
                totalCheckout.Content = total * (100 - discount) / 100;
            }
            else
            {
                totalCheckout.Content = total;
            }
            
            

        }

        private void Discount_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (TotalBeforeDiscount.Content != null)
            {
                int total = int.Parse(TotalBeforeDiscount.Content.ToString());
                if (!String.IsNullOrEmpty(Discount.Text))
                {
                    if (_regex.IsMatch(Discount.Text) || 0 > int.Parse(Discount.Text)
                    || int.Parse(Discount.Text) > 100)
                    {
                        totalCheckout.Content = total;
                        Discount.Text = null;
                        MessageBox.Show("Vui lòng nhập số lớn hơn 0 và nhỏ hơn 100 hoặc không để trống.");
                    }
                    else
                    {
                        int discount = int.Parse(Discount.Text);
                        totalCheckout.Content = total * (100 - discount) / 100;
                    }
                }
                else
                {
                    totalCheckout.Content = total;
                }
            }
            else
            {
                if(!String.IsNullOrEmpty(Discount.Text))
                {
                    if (_regex.IsMatch(Discount.Text) || 0 > int.Parse(Discount.Text)
                    || int.Parse(Discount.Text) > 100)
                    {
                        Discount.Text = null;
                        MessageBox.Show("Vui lòng nhập số lớn hơn 0 và nhỏ hơn 100 hoặc không để trống.");
                    }
                }
                
            }
        }

        private async void Check_Out(object sender, RoutedEventArgs e)
        {
            if(listProductCart.Count == 0)
            {
                MessageBox.Show("Vui lòng gọi đồ trước khi checkout!");
            }
            else
            {
                int discount = 0;
                int totalAfterDiscount = int.Parse(totalCheckout.Content.ToString());
                if (!String.IsNullOrEmpty(Discount.Text.ToString()))
                {
                    discount = int.Parse(Discount.Text.ToString());
                }
                int totalBeforeDiscount = int.Parse(TotalBeforeDiscount.Content.ToString());
                BillRequest bill = new BillRequest();
                bill.Discount = discount;
                bill.DateCheckOut = DateTime.Now;
                bill.DateCheckIn = DateTime.Now;
                bill.IdAccount = account.Id;
                bill.total = totalAfterDiscount;
                CheckoutRequest checkoutRequest = new CheckoutRequest();
                checkoutRequest.Bill = bill;
                checkoutRequest.ListProduct= listProductCart;
                checkoutRequest.totalCheckout = totalAfterDiscount;
                CheckoutResponse checkoutResponse = await cartManagement.CheckOut(checkoutRequest);
                BillDetail billDetail = new BillDetail(listProductCart, account, totalAfterDiscount, discount, totalBeforeDiscount, this, checkoutResponse.bill.Id, checkoutResponse);
                billDetail.Show();
            }
            
        }

        public void reload()
        {
            ProductSearchRequest productFilter = new ProductSearchRequest();
            loadData(productFilter);
            ListCart.ItemsSource = null;
            totalCheckout.Content = null;
            TotalBeforeDiscount.Content = null;
            Discount.Text = null;
            searchByName.Text = null;
            listProductCart.Clear();
        }

        private void ListProduct_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
