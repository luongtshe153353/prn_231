﻿using Microsoft.Extensions.DependencyInjection;
using OiHouseCoffee;
using OiHouseCoffee.Data_Layer.Repository;
using System.Windows;

namespace OiHouseCoffe_Client
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private ServiceProvider serviceProvider;

        public App()
        {
            ServiceCollection services = new ServiceCollection();
            ConfigureServices(services);
            serviceProvider = services.BuildServiceProvider();
        }

        private void ConfigureServices(ServiceCollection services)
        {
            services.AddSingleton<AdminManager>();
            //services.AddSingleton<Home>();
            services.AddSingleton<MainWindow>();
            //services.AddSingleton<EmployeeManeger>();
            //services.AddSingleton<ProductManager>();
        }

        private void OnStartup(object sender, StartupEventArgs e)
        {
            var mainWindow = serviceProvider.GetService<MainWindow>();
            if (mainWindow != null)
            {
                mainWindow.Show();
            }
        }
    }
}
