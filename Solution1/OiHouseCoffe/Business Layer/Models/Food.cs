﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace OiHouseCoffee.Business_Layer.Models;

public partial class Food
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public int IdCategory { get; set; }

    public int Price { get; set; }

    public virtual ICollection<BillInfo> BillInfos { get; set; } = new List<BillInfo>();

    public virtual FoodCategory IdCategoryNavigation { get; set; } = null;
    public string? Image { get; set; }


    public bool? Status { get; set; }

    

}
