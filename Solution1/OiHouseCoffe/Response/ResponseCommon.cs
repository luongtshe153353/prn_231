﻿namespace OiHouseCoffee.Response
{
    public class ResponseCommon
    {
        public int code { get; set; }

        public string msg { get; set; }

    }
}
