﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using OiHouseCoffe_Client.Logic;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Data_Layer.Repository;
using OiHouseCoffee.Request;

namespace OiHouseCoffee
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        LoginManagement loginManagement = new LoginManagement();
        

        public MainWindow()
        {

            InitializeComponent();
           
        }

        public void resetFormLogin()
        {
            textBoxUserName.Text = null;
            textBoxPassWord.Password = null;
        }
        private void textBoxUserName_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            string username = textBoxUserName.Text.ToString();
            string password = textBoxPassWord.Password.ToString();
            AccountRequest account = new AccountRequest();
            account.UserName = username;
            account.PassWord = password;


            if (!String.IsNullOrEmpty(username) && !String.IsNullOrEmpty(password))
            {   Account acc = await loginManagement.Login(account);
                if(acc != null)
                {
                    

                    if (acc.Status != null)
                    {
                        Home home = new Home(acc);
                        this.Close();
                        home.Show();
                        resetFormLogin();
                    }
                    else
                    {
                        AdminManager adminManager = new AdminManager();
                        this.Close();
                        adminManager.Show();
                        resetFormLogin();
                    }
                    
                }
                else
                {
                    MessageBox.Show(" username and password isn't correct");
                }
            }
            else
            {
                MessageBox.Show("Please enter username and password");
            }
        }
    }
}
