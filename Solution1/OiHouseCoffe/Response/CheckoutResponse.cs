﻿using OiHouseCoffee.Business_Layer.Models;

namespace OiHouseCoffee.Response
{
    public class CheckoutResponse
    {
        public byte[] imageQR { get; set; }

        public Bill bill { get; set; }

    }
}
