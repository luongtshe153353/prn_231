﻿
using OiHouseCoffee.Business_Layer.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Windows;

namespace OiHouseCoffee.Data_Layer
{
    public class AccountDAO
    {
        private static AccountDAO instance = null;
        private static readonly object instanceLock = new object();

        private AccountDAO() {
        }

        public static AccountDAO Instance
        {
            get
            {
                lock (instanceLock)
                {
                    if (instance == null)
                    {
                        instance = new AccountDAO();
                    }
                    return instance;
                }

            }
        }

        public void Add(Account Account)
        {
            try
            {
                
                    using (var saleManagement = new QuanLyQuanCafeContext())
                    {
                        saleManagement.Accounts.Add(Account);
                        saleManagement.SaveChanges();
                    }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Delete(Account Account)
        {
            try
            {
                Account accountDelete = FindOne(item => item.Id.Equals(Account.Id));
                if (accountDelete != null)
                {
                    using (var dbContext = new QuanLyQuanCafeContext())
                    {
                        dbContext.Accounts.Remove(accountDelete);
                        dbContext.SaveChanges();
                    }
                }
                else
                {
                    throw new Exception("The Account does not exist");
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public Account FindOne(Expression<Func<Account, bool>> predicate)
        {
            Account Account = null;
            try
            {
                using (var dbContext = new QuanLyQuanCafeContext())
                {   
                        Account = dbContext.Accounts.FirstOrDefault(predicate);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return Account;
        }

        public IEnumerable<Account> FindAll(Expression<Func<Account, bool>> predicate)
        {
            List<Account> Accounts = new List<Account>();
            try
            {
                using (var dbContext = new QuanLyQuanCafeContext())
                {
                    Accounts = dbContext.Accounts.Where(predicate).ToList();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return Accounts;
        }

        public IEnumerable<Account> List()
        {
            List<Account> Accounts = new List<Account>();
            try
            {
                using (var dbContext = new QuanLyQuanCafeContext())
                {
                    Accounts = dbContext.Accounts.ToList();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return Accounts;
        }

        public void Update(Account Account)
        {
            try
            {
                Account accountNew = FindOne(item => item.Id.Equals(Account.Id));
                if (accountNew != null)
                {
                    using (var saleManager = new QuanLyQuanCafeContext())
                    {
                        saleManager.Entry<Account>(Account).State = EntityState.Modified;
                        saleManager.SaveChanges();
                    }
                }
                else
                {
                    throw new Exception("The Account does not exist");
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void updateStatusUser(int Id, bool status)
        {
            try
            {
                Account accountNew = FindOne(item => item.Id.Equals(Id));
                if (accountNew != null)
                {
                    accountNew.Status = status;
                    using (var saleManager = new QuanLyQuanCafeContext())
                    {
                        saleManager.Entry<Account>(accountNew).State = EntityState.Modified;
                        saleManager.SaveChanges();
                    }
                }
                else
                {
                    throw new Exception("The Account does not exist");
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        

    }
}
