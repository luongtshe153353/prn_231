﻿#pragma checksum "..\..\..\..\FormCommon\Home.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "F8810FCF65113B8836978F9E29B08FBE07721152"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace OiHouseCoffee {
    
    
    /// <summary>
    /// Home
    /// </summary>
    public partial class Home : System.Windows.Window, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 25 "..\..\..\..\FormCommon\Home.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox searchByName;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\..\FormCommon\Home.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox comboBoxCategory;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\..\FormCommon\Home.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView ListProduct;
        
        #line default
        #line hidden
        
        
        #line 110 "..\..\..\..\FormCommon\Home.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView ListCart;
        
        #line default
        #line hidden
        
        
        #line 172 "..\..\..\..\FormCommon\Home.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label TotalBeforeDiscount;
        
        #line default
        #line hidden
        
        
        #line 180 "..\..\..\..\FormCommon\Home.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Discount;
        
        #line default
        #line hidden
        
        
        #line 188 "..\..\..\..\FormCommon\Home.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label totalCheckout;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "7.0.3.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/OiHouseCoffe_Client;component/formcommon/home.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\FormCommon\Home.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "7.0.3.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 7 "..\..\..\..\FormCommon\Home.xaml"
            ((OiHouseCoffee.Home)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Page_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.searchByName = ((System.Windows.Controls.TextBox)(target));
            
            #line 25 "..\..\..\..\FormCommon\Home.xaml"
            this.searchByName.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.searchByName_TextChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            this.comboBoxCategory = ((System.Windows.Controls.ComboBox)(target));
            
            #line 32 "..\..\..\..\FormCommon\Home.xaml"
            this.comboBoxCategory.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.comboBoxCategory_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 4:
            
            #line 38 "..\..\..\..\FormCommon\Home.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Check_Out);
            
            #line default
            #line hidden
            return;
            case 5:
            this.ListProduct = ((System.Windows.Controls.ListView)(target));
            
            #line 45 "..\..\..\..\FormCommon\Home.xaml"
            this.ListProduct.PreviewMouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.Control_MouseDoubleClick);
            
            #line default
            #line hidden
            
            #line 45 "..\..\..\..\FormCommon\Home.xaml"
            this.ListProduct.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.ListProduct_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.ListCart = ((System.Windows.Controls.ListView)(target));
            return;
            case 10:
            this.TotalBeforeDiscount = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.Discount = ((System.Windows.Controls.TextBox)(target));
            
            #line 180 "..\..\..\..\FormCommon\Home.xaml"
            this.Discount.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.Discount_TextChanged);
            
            #line default
            #line hidden
            return;
            case 12:
            this.totalCheckout = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "7.0.3.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 7:
            
            #line 143 "..\..\..\..\FormCommon\Home.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Remove_Click);
            
            #line default
            #line hidden
            break;
            case 8:
            
            #line 145 "..\..\..\..\FormCommon\Home.xaml"
            ((System.Windows.Controls.TextBox)(target)).TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.Quantity_TextChanged);
            
            #line default
            #line hidden
            break;
            case 9:
            
            #line 146 "..\..\..\..\FormCommon\Home.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Add_Click);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}

