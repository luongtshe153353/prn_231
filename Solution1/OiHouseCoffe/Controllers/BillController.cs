﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Data_Layer.Repository;
using OiHouseCoffee.Request;
using OiHouseCoffee.Response;
using OiHouseCoffee.Service;
using System.Collections.Generic;

namespace OiHouseCoffee.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BillController : ControllerBase
    {
        private ProductService productService = new ProductService();

        private BillRepository billRepository = new BillRepository();

        private OrderInfoRepository orderInfoRepository = new OrderInfoRepository();

        private IMapper _mapper;
        public BillController(IMapper mapper)
        {
            _mapper = mapper;
        }

        [HttpPost]
        [Route("search")]
        public IActionResult search([FromBody] BillSearchRequest billSearchRequest)
        {
            List<Bill> bills = (List<Bill>)billRepository.FindAllByStartTimeAndEndTime(billSearchRequest);
            return Ok(_mapper.Map<List<BillResponse>>(bills));
        }

        [HttpGet]
        [Route("findById/{id}")]
        public IActionResult search(int id)
        {
            List<BillInfo> bills = (List<BillInfo>)orderInfoRepository.FindAllById(id);
            return Ok(_mapper.Map<List<BillInfoReponse>>(bills));
        }
    }
}
