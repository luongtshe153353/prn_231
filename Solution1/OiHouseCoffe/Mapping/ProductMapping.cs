﻿using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Response;
using AutoMapper;
using OiHouseCoffee.Request;

namespace OiHouseCoffee.Mapping
{
    public class ProductMapping : Profile
    {
        public ProductMapping() 
        {
            CreateMap<Food, ResponseProduct>();
            CreateMap<ResponseProduct, Food>();
            CreateMap<Food, ProductRequest>();
            CreateMap<ProductRequest, Food>();

        }
    }
}
