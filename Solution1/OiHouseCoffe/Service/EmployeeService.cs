﻿using Microsoft.AspNetCore.Http.HttpResults;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Data_Layer.Repository;
using OiHouseCoffee.Response;

namespace OiHouseCoffee.Service
{
    public class EmployeeService
    {
        private AccountRepository accountRepository = new AccountRepository();

        public ResponseCommon addEmployee(Account account)
        {
            ResponseCommon reponse = new ResponseCommon();
            Account accountFindByEmail = accountRepository.FindByEmail(account.Email);
            Account accountFindByPhone = accountRepository.FindByPhoneNumber(account.PhoneNumber);
            Account accountFindByUserName = accountRepository.FindByName(account.UserName);
            string mess = null;
            if (accountFindByEmail != null)
            {
                mess += "Email";
            }
            if (accountFindByPhone != null)
            {   
                if(mess != null)
                {
                    mess += ", ";
                }
                mess += "Phone Number";
            }
            if (accountFindByUserName != null)
            {
                if (mess != null)
                {
                    mess += ", ";
                }
                mess += "UserName";
            }
            if(mess != null)
            {
                mess += " đã tồn tại!";
                reponse.msg = mess;
            }
            else
            {
                reponse.code = (int)Contant.SUCCESS;
                accountRepository.Add(account);
            }
            return reponse;

        }

        public ResponseCommon updateEmployee(Account account)
        {
            ResponseCommon reponse = new ResponseCommon();
            Account accountFindByEmail = accountRepository.FindByEmail(account.Email);
            Account accountFindByPhone = accountRepository.FindByPhoneNumber(account.PhoneNumber);
            Account accountFindByUserName = accountRepository.FindByName(account.UserName);
            string mess = null;
            if (accountFindByEmail != null && !account.Id.Equals(accountFindByEmail.Id))
            {
                mess += "Email";
            }
            if (accountFindByPhone != null && !account.Id.Equals(accountFindByPhone.Id))
            {
                if (mess != null)
                {
                    mess += ", ";
                }
                mess += "Phone Number";
            }
            if (accountFindByUserName != null && !account.Id.Equals(accountFindByUserName.Id))
            {
                if (mess != null)
                {
                    mess += ", ";
                }
                mess += "UserName";
            }
            if (mess != null)
            {
                mess += " đã tồn tại!";
                reponse.msg = mess;
            }
            else
            {
                reponse.code = (int)Contant.SUCCESS;
                accountRepository.Update(account);
            }
            return reponse;

        }

       public ResponseCommon updateStatusUser(int id, bool status)
        {
            ResponseCommon reponse = new ResponseCommon();
            reponse.code = (int)Contant.SUCCESS;
            accountRepository.updateStatusUser(id, status);
            return reponse;
        }
    }
}
