﻿using OiHouseCoffee.Business_Layer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OiHouseCoffee.Business_Layer.Models
{
    public class ProductCart
    {
        public Food product { get; set; }

        public int quantity { get; set; }

        public int price { get; set; }


        public void setPrice()
        {
            this.price = this.product.Price * this.quantity;
        }

        public void setPrice(int quantity)
        {
            this.quantity = quantity;
            this.price = this.product.Price * quantity;
        }

        public ProductCart(Food food, int quantity)
        {
            this.product = food;
            this.quantity = quantity;
            this.price = product.Price * quantity;
        }

        public bool Equals(Food _product)
        {
            if (this.product.Id != _product.Id) return false;
            return true;
        }

        public override bool Equals(object obj)
        {
            if (obj is not Food) return false;
            if (obj == null) return false;
            return Equals(obj as Food);
        }

        public override int GetHashCode()
        {
            return product.Id;
        }

    }
}
