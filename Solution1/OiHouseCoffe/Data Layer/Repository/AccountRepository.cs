﻿using OiHouseCoffee.Business_Layer.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using OiHouseCoffee.Request;

namespace OiHouseCoffee.Data_Layer.Repository
{
    
    public class AccountRepository 
    {
        public void Add(Account account)
        {
            AccountDAO.Instance.Add(account);
        }

        public void Delete(Account account)
        {
            AccountDAO.Instance.Delete(account);
        }

        public IEnumerable<Account> FindAllBy(EmployeeSearchRequest filter)
        {
            if (String.IsNullOrEmpty(filter.Email) && String.IsNullOrEmpty(filter.UserName) && String.IsNullOrEmpty(filter.PhoneNumber) && filter.AccountId == null)
            {
                return GetAll();
            }
            else
            {
                return AccountDAO.Instance.FindAll(account => (filter.AccountId == null || account.Id.Equals(filter.AccountId)) &&
                                                              (filter.Email == null || account.Email.ToLower().Contains(filter.Email.ToLower())) &&
                                                              (filter.UserName == null || account.UserName.ToLower().Contains(filter.UserName.ToLower())) &&
                                                              (filter.PhoneNumber == null || account.PhoneNumber.Contains(filter.PhoneNumber)));
            }

        }

        public Account FindByEmail(string email)
        {
            return AccountDAO.Instance.FindOne(account => account.Email == email);
        }

        public Account FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Account FindByName(string name)
        {
            return AccountDAO.Instance.FindOne(account => account.UserName == name);
        }

        public Account FindByPasswordAndUserName(string password, string username)
        {
            return AccountDAO.Instance.FindOne(Account => Account.UserName == username && Account.PassWord == password);
        }

        public Account FindByPhoneNumber(string phoneNumber)
        {
            return AccountDAO.Instance.FindOne(account => account.PhoneNumber == phoneNumber);
        }

        public IEnumerable<Account> GetAll()
        {
            return AccountDAO.Instance.List();
        }

        public void Update(Account account)
        {
            AccountDAO.Instance.Update(account);
        }

        public void updateStatusUser(int id, bool status) {

            AccountDAO.Instance.updateStatusUser(id, status);
        }
       
    }
}
