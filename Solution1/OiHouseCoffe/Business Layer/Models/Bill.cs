﻿using System;
using System.Collections.Generic;

namespace OiHouseCoffee.Business_Layer.Models;

public partial class Bill
{
    public int Id { get; set; }

    public DateTime DateCheckIn { get; set; }

    public DateTime? DateCheckOut { get; set; }

    public int total { get; set; }

    public int? Discount { get; set; }

    public int IdAccount { get; set; }

    public virtual ICollection<BillInfo> BillInfos { get; set; } = new List<BillInfo>();

    public virtual Account? IdAccountNavigation { get; set; }

}
