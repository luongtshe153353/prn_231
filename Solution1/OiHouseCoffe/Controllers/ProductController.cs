﻿using AutoMapper;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Data_Layer.Repository;
using OiHouseCoffee.Request;
using OiHouseCoffee.Response;
using OiHouseCoffee.Service;

namespace OiHouseCoffee.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private ProductRepository productRepository = new ProductRepository();

        private ProductService productService = new ProductService();

        private IMapper _mapper;

        public ProductController(IMapper mapper)
        {
            _mapper = mapper;
        }

        [HttpPost]
        [Route("search")]
        public IActionResult search([FromBody] ProductSearchRequest productSearchRequest)
        {
            List<Food> products = (List<Food>)productRepository.FindAllBy(productSearchRequest);
            return Ok(_mapper.Map<List<ResponseProduct>>(products));
        }

        [HttpPost]
        [Route("addProduct")]
        public IActionResult addProduct([FromBody] ProductRequest food)
        {
            ResponseCommon reponse = new ResponseCommon();
            try
            {
                try
                {
                    return Ok(productService.addProduct(food));
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
            catch (Exception e)
            {
                return Ok(Contant.ERROR);
            }
        }

        [HttpPut]
        [Route("updateProduct")]
        public IActionResult updateProduct([FromBody] Food food)
        {
            try
            {
                try
                {
                    return Ok(productService.updateProduct(food));
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("updateStatusFood")]
        public IActionResult deactiveAccount([FromBody] RequestUpdateStatus requestUpdateStatus)
        {
            try
            {
                try
                {
                    return Ok(productService.updateStatusProduct(requestUpdateStatus.id, requestUpdateStatus.status));
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }

    
}
