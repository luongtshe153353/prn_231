﻿using OiHouseCoffee.Business_Layer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OiHouseCoffee.Data_Layer
{
    public class FoodCategoryDao
    {
        private static FoodCategoryDao instance = null;
        private static readonly object instanceLock = new object();


        public static FoodCategoryDao Instance
        {
            get
            {
                lock (instanceLock)
                {
                    if (instance == null)
                    {
                        instance = new FoodCategoryDao();
                    }
                    return instance;
                }

            }
        }

        public IEnumerable<FoodCategory> List()
        {
            List<FoodCategory> FoodCategories = new List<FoodCategory>();
            try
            {
                using (var dbContext = new QuanLyQuanCafeContext())
                {
                    FoodCategories = dbContext.FoodCategories.ToList();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return FoodCategories;
        }
    }
}
