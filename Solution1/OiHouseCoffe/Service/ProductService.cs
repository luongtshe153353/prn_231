﻿using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Data_Layer.Repository;
using OiHouseCoffee.Request;
using OiHouseCoffee.Response;

namespace OiHouseCoffee.Service
{
    public class ProductService
    {
        private ProductRepository productRepository = new ProductRepository();

        public ResponseCommon addProduct(ProductRequest food)
        {
            ResponseCommon reponse = new ResponseCommon();

            Food productFindByName = productRepository.FindByName(food.Name);
            string mess = null;
            if (productFindByName != null)
            {
                mess = "Name product đã tồn tại!";
                reponse.msg = mess;
            }
            else
            {
                Food foodNew = new Food();
                foodNew.Name = food.Name;
                foodNew.Status = food.Status;
                foodNew.Price = food.Price;
                foodNew.IdCategory = food.IdCategory;
                foodNew.Image = food.Image;
                productRepository.Add(foodNew);
                reponse.code = (int)Contant.SUCCESS;
            }
            return reponse;
        }

        public ResponseCommon updateProduct(Food food)
        {
            ResponseCommon reponse = new ResponseCommon();

            Food productFindByName = productRepository.FindByName(food.Name);
            string mess = null;
            if (productFindByName != null && productFindByName.Id != food.Id)
            {
                mess = "Name product đã tồn tại!";
                reponse.msg = mess;
            }
            else
            {
                productFindByName.Name = food.Name;
                productFindByName.Status = food.Status;
                productFindByName.Price = food.Price;
                productFindByName.IdCategory = food.IdCategory;
                productFindByName.Image = food.Image;
                productRepository.Update(productFindByName);
                reponse.code = (int)Contant.SUCCESS;
            }
            return reponse;

        }

       public ResponseCommon updateStatusProduct(int id, bool status)
        {
            ResponseCommon reponse = new ResponseCommon();
            reponse.code = (int)Contant.SUCCESS;
            productRepository.updateStatusFood(id, status);
            return reponse;
        }
    }
}
