﻿using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OiHouseCoffee.Data_Layer.Repository
{
    public class BillRepository 
    {
        public void Add(Bill bill)
        {
            BillDAO.Instance.Add(bill);
        }

        public void Delete(Bill bill)
        {
            throw new NotImplementedException();
        }

        //public IEnumerable<Bill> FindAllBy(BillFilter filter)
        //{
        //    throw new NotImplementedException();
        //}

        //public IEnumerable<Bill> FindAllByAccount(string username)
        //{
        //    throw new NotImplementedException();
        //}

        public IEnumerable<Bill> FindAllByStartTimeAndEndTime(BillSearchRequest billSearchRequest)
        {
            if(billSearchRequest.StartDate == null && billSearchRequest.EndDate == null)
            {
                return BillDAO.Instance.List();
            }
            else
            {
                return BillDAO.Instance.FindAll(order => (billSearchRequest.StartDate == null || order.DateCheckOut >= billSearchRequest.StartDate) &&
                                                      (billSearchRequest.EndDate == null || order.DateCheckOut <= billSearchRequest.EndDate) ||
                                                       (billSearchRequest.StartDate != null && billSearchRequest.EndDate != null
                                                       && order.DateCheckOut >= billSearchRequest.StartDate && order.DateCheckOut <= billSearchRequest.EndDate))
                                                       .OrderByDescending(order => order.DateCheckOut).ToList();
            }
            
        }

        public Bill Get(int id)
        {
            throw new NotImplementedException();
        }



        //public IEnumerable<Bill> GetLastProduct() {
        //    Bill bill = new Bill();
        //    return bill;
        //}

        public IEnumerable<Bill> GetAll()
        {
            return BillDAO.Instance.List();
        }

        public void Update(Bill bill)
        {
            throw new NotImplementedException();
        }
    }
}
