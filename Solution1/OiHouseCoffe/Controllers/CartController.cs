﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Request;
using OiHouseCoffee.Response;
using OiHouseCoffee.Service;

namespace OiHouseCoffee.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        CartService cartService = new CartService();

        [HttpPost]
        [Route("checkout")]
        public IActionResult checkout([FromBody] CheckoutRequest checkoutRequest)
        {
            ResponseCommon reponse = new ResponseCommon();
                try
                {
                    return Ok(cartService.checkout(checkoutRequest));
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }

        }
    }
}
