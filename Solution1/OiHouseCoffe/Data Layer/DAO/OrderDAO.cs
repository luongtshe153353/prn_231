﻿
using OiHouseCoffee.Business_Layer.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace OiHouseCoffee.Data_Layer
{
    public class BillDAO
    {
        private static BillDAO instance = null;
        private static readonly object instanceLock = new object();

        private BillDAO() { }
        public static BillDAO Instance
        {
            get
            {
                lock (instanceLock)
                {
                    if (instance == null)
                    {
                        instance = new BillDAO();
                    }
                    return instance;
                }

            }
        }

        public IEnumerable<Bill> List()
        {
            List<Bill> Bills = new List<Bill>();
            try
            {
                using (var dbContext = new QuanLyQuanCafeContext())
                {
                    Bills = dbContext.Bills.Include(x => x.IdAccountNavigation).ToList();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return Bills;
        }


        public Bill FindOne(Expression<Func<Bill, bool>> predicate)
        {
            Bill bill = null;
            try
            {
                using (var dbContext = new QuanLyQuanCafeContext())
                {
                    bill = dbContext.Bills.SingleOrDefault(predicate);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return bill;
        }

        public IEnumerable<Bill> FindAll(Expression<Func<Bill, bool>> predicate)
        {
            List<Bill> Bills = new List<Bill>();
            using (var dbContext = new QuanLyQuanCafeContext())
            {
                Bills = dbContext.Bills.Include(x=> x.IdAccountNavigation).Where(predicate).ToList();
            }
            return Bills;
        }

        public void Add(Bill bill)
        {
            try
            {
                    using (var saleManagement = new QuanLyQuanCafeContext())
                    {
                        saleManagement.Bills.Add(bill);
                        saleManagement.SaveChanges();
                    }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Delete(Bill bill)
        {
            try
            {
                Bill billDelete = FindOne(item => item.Id.Equals(bill.Id));
                if (billDelete != null)
                {
                    using (var dbContext = new QuanLyQuanCafeContext())
                    {
                        dbContext.Bills.Remove(billDelete);
                        dbContext.SaveChanges();
                    }
                }
                else
                {
                    throw new Exception("The Bill does not exist");
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Update(Bill bill)
        {
            try
            {
                Bill billNew = FindOne(item => item.Id.Equals(bill.Id));
                if (billNew != null)
                {
                    using (var saleManager = new QuanLyQuanCafeContext())
                    {
                        saleManager.Entry<Bill>(billNew).State = EntityState.Modified;
                        saleManager.SaveChanges();
                    }
                }
                else
                {
                    throw new Exception("The Bill does not exist");
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }


        public IEnumerable<Bill> getLastProduct()
        {
            List<Bill> Bills = new List<Bill>();
            try
            {
                using (var dbContext = new QuanLyQuanCafeContext())
                {
                    Bills = dbContext.Bills.ToList();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return Bills;
        }
    }
}
