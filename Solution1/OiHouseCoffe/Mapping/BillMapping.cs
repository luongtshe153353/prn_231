﻿using AutoMapper;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Request;
using OiHouseCoffee.Response;

namespace OiHouseCoffee.Mapping
{
    public class BillMapping : Profile
    {
        public BillMapping()
        {
            CreateMap<Bill, BillRequest>();
            CreateMap<BillRequest, Bill>();
            CreateMap<BillResponse, Bill>();
            CreateMap<Bill, BillResponse>();

        }
    }
}
