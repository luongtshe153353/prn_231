﻿using Newtonsoft.Json;
using OiHouseCoffee.Business_Layer.Models;
using OiHouseCoffee.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace OiHouseCoffe_Client.Logic
{
    class CategoryManagement
    {
        private readonly HttpClient client = null;
        private string url = "";

        public CategoryManagement()
        {
            client = new HttpClient();
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);
            client.DefaultRequestHeaders.Accept.Clear();
            url = "http://localhost:5123/api/category";
        }

        public async Task<List<FoodCategory>> Search()
        {

            HttpResponseMessage response = await client.GetAsync($"{url}/search");

            string strdata = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            List<FoodCategory> foodCategories = System.Text.Json.JsonSerializer.Deserialize<List<FoodCategory>>(strdata, options).ToList();
            return foodCategories;
        }
    }
}
