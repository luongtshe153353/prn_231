﻿using OiHouseCoffee.Business_Layer.Models;

namespace OiHouseCoffee.Response
{
    public class BillInfoReponse
    {
        public int Id { get; set; }

        public int IdBill { get; set; }

        public int IdFood { get; set; }

        public int Count { get; set; }

        public int price { get; set; }

    }
}
